package se.plushogskolan.web.filters;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import se.plushogskolan.web.domain.cars.Car;
import se.plushogskolan.web.domain.cars.CarService;
import se.plushogskolan.web.util.CookieWrapper;

/**
 * Servlet Filter implementation class NewCars
 */
public class EditedCars implements Filter {

    /**
     * @see Filter#destroy()
     */
    public void destroy() {
    }

    /**
     * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
     */
    @SuppressWarnings("unchecked")
    public void doFilter(ServletRequest req, ServletResponse resp,
            FilterChain chain) throws IOException, ServletException {

        List<Car> carList = new ArrayList<Car>();

        HttpServletRequest request = (HttpServletRequest) req;

        Cookie cookie = CookieWrapper.getCookieByName(request, "edited_cars");

        if (cookie != null) {
            List<String> list = new ArrayList<String>(Arrays.asList(cookie
                    .getValue().split(":")));

            for (String regno : list) {
                carList.add(CarService.getInstance().getCarByRegNo(regno));
            }

        }
        else {
            HttpSession session = request.getSession();

            List<String> list = (List<String>) session
                    .getAttribute("edited_cars");

            if (list != null) {
                for (String regno : list) {
                    carList.add(CarService.getInstance().getCarByRegNo(regno));
                }
            }
        }
        Collections.reverse(carList);
        req.setAttribute("edited_cars", carList);
        chain.doFilter(req, resp);
    }

    @Override
    public void init(FilterConfig arg0) throws ServletException {
    }

}
