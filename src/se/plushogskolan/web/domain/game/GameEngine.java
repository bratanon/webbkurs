package se.plushogskolan.web.domain.game;

/* A singleton Game engine */
public class GameEngine {

	private static GameEngine engine;

	private Hero hero = Hero.TROLL;
	private int speed = 5;
	private int xPos = 400;
	private int yPos = 200;
	private int gameWidth = 800;
	private int gameHeight = 475;

	private GameEngine() {

	}

	public static GameEngine getInstance() {
		if (engine == null) {
			engine = new GameEngine();
		}
		return engine;
	}

	public Hero getHero() {
		return hero;
	}

	public void setHero(Hero hero) {
		this.hero = hero;
	}

	public int getxPos() {
		return xPos;
	}

	public void setxPos(int xPos) {
		this.xPos = xPos;
	}

	public int getyPos() {
		return yPos;
	}

	public void setyPos(int yPos) {
		this.yPos = yPos;
	}

	public int getGameWidth() {
		return gameWidth;
	}

	public void setGameWidth(int gameWidth) {
		this.gameWidth = gameWidth;
	}

	public int getGameHeight() {
		return gameHeight;
	}

	public void setGameHeight(int gameHeight) {
		this.gameHeight = gameHeight;
	}

	public int getSpeed() {
		return speed;
	}

	public void setSpeed(int speed) {
		this.speed = speed;
	}

	public void moveLeft() {
		MoveToPosition(getxPos() - getSpeed(), getyPos());
	}

	public void moveRight() {
		MoveToPosition(getxPos() + getSpeed(), getyPos());
	}

	public void moveTop() {
		MoveToPosition(getxPos(), getyPos() - getSpeed());
	}

	public void moveBottom() {
		MoveToPosition(getxPos(), getyPos() + getSpeed());
	}

	public void MoveToPosition(int x, int y) {

		if (x < 0 || y < 0) {
			// X or Y too small, ignore move
			return;
		}

		if (x + getHero().getWidth() > getGameWidth()) {
			// X too large, ignore move
			return;
		}

		if (y + getHero().getHeigh() > getGameHeight()) {
			// y too large, ignore move
			return;
		}

		// We are within valid boundaries, change position
		setxPos(x);
		setyPos(y);

	}
}
