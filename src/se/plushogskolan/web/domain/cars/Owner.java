package se.plushogskolan.web.domain.cars;

import java.util.HashSet;
import java.util.Set;

public class Owner {
	private String socialSecurityNo;
	private String firstName;
	private String lastName;
	private String address;
	private String zipCode;
	private String city;
	private Set<Car> cars = new HashSet<Car>();

	public Owner() {

	}

	public Owner(String socialSecurityNo, String firstName, String lastName, String address, String zipCode, String city) {
		setSocialSecurityNo(socialSecurityNo);
		setFirstName(firstName);
		setLastName(lastName);
		setAddress(address);
		setZipCode(zipCode);
		setCity(city);

	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Set<Car> getCars() {
		return cars;
	}

	public void setCars(Set<Car> cars) {
		this.cars = cars;
	}

	public String getSocialSecurityNo() {
		return socialSecurityNo;
	}

	public void setSocialSecurityNo(String socialSecurityNo) {
		this.socialSecurityNo = socialSecurityNo;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getFullName() {

		return getFirstName() + " " + getLastName();
	}

	@Override
	public String toString() {
		return "Owner [socialSecurityNo=" + socialSecurityNo + ", firstName=" + firstName + ", lastName=" + lastName
				+ ", no of cars=" + cars.size() + "]";
	}

}
