package se.plushogskolan.web.domain.cars;

/**
 * An enum representing a car brand.
 */
public enum Brand {

	VOLVO("Volvo", "volvo_200.jpg", "volvo_100.jpg"), MERCEDES("Mercedes", "mercedes_200.jpg", "mercedes_100.jpg"), RENAULT(
			"Renault", "renault_200.jpg", "renault_100.jpg"), VW("Volkswagen", "volkswagen_200.jpg",
			"volkswagen_100.jpg");

	private String niceName;
	private String logo200px;
	private String logo100px;

	private Brand(String niceName, String logo200, String logo100) {
		this.niceName = niceName;
		this.logo200px = logo200;
		this.logo100px = logo100;
	}

	public String getNiceName() {
		return niceName;
	}

	public String getLogo100px() {
		return logo100px;
	}

	public String getLogo200px() {
		return logo200px;
	}

}
