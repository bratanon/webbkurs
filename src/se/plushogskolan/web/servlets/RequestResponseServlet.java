package se.plushogskolan.web.servlets;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

/**
 * Servlet implementation class RequestResponseServlet
 */
@WebServlet("/RequestResponseServlet")
public class RequestResponseServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    private Logger log = Logger.getLogger(this.getClass().getName());

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
     *      response)
     */
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {
        RequestDispatcher dispatcher = request
                .getRequestDispatcher("/WEB-INF/jsp/requestResponse.jsp");

        if (request.getParameter("id") != null) {
            switch (request.getParameter("id")) {
                case "1":
                    request.setAttribute("user", "Kalle");
                    break;
                case "2":
                    request.setAttribute("user", "Stina");
                    break;
            }
        }

        response.setHeader("Last-Modified", "Fri, 15 Nov 2013 12:45:26 +0000");

        dispatcher.forward(request, response);
    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
     *      response)
     */
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {
        RequestDispatcher dispatcher = request
                .getRequestDispatcher("/WEB-INF/jsp/requestResponse.jsp");
        dispatcher.forward(request, response);
    }

}
