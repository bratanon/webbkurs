package se.plushogskolan.web.servlets;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import se.plushogskolan.web.domain.cars.Car;
import se.plushogskolan.web.domain.cars.CarService;

/**
 * Servlet implementation class ExpressionLanguage
 */
public class ExpressionLanguage extends HttpServlet {
    private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
     *      response)
     */
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {
        RequestDispatcher dispatcher = request
                .getRequestDispatcher("/WEB-INF/jsp/el.jsp");

        se.plushogskolan.web.domain.cars.CarService carService = CarService
                .getInstance();
        Car car = carService.getCarByRegNo("ABC-123");

        request.setAttribute("car", car);

        request.setAttribute("message",
                "This is an info message in the request scope.");
        request.getSession().setAttribute("message",
                "This is an info message in the session scope.");
        request.getServletContext().setAttribute("message",
                "This is an info message in the application scope.");

        dispatcher.forward(request, response);
    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
     *      response)
     */
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {
        RequestDispatcher dispatcher = request
                .getRequestDispatcher("/WEB-INF/jsp/el.jsp");
        dispatcher.forward(request, response);
    }

}
