package se.plushogskolan.web.servlets;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import se.plushogskolan.web.domain.game.GameEngine;
import se.plushogskolan.web.domain.game.Hero;

/**
 * Servlet implementation class game
 */
public class game extends HttpServlet {
    private static final long serialVersionUID = 1L;

    private Logger log = Logger.getLogger(this.getClass().getName());

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
     *      response)
     */
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {

        GameEngine game = GameEngine.getInstance();

        if (request.getParameter("hero") != null) {
            game.setHero(getHeroFromParam(request.getParameter("hero")));
        }

        if (request.getParameter("dir") != null) {
            switch (request.getParameter("dir")) {
                case "up":
                    game.moveTop();
                    break;

                case "down":
                    game.moveBottom();
                    break;

                case "left":
                    game.moveLeft();
                    break;

                case "right":
                    game.moveRight();
                    break;
            }
        }

        if (request.getParameter("speed") != null) {
            game.setSpeed(Integer.parseInt(request.getParameter("speed")));
        }

        request.setAttribute("game", game);

        RequestDispatcher dispatcher = request
                .getRequestDispatcher("/WEB-INF/jsp/game.jsp");
        dispatcher.forward(request, response);
    }

    /**
     * Gets a hero.
     * 
     * @param stringHero
     *            name of the here to get.
     * @return a Hero enum. Defaul to TROLL if no other is found.
     */
    private Hero getHeroFromParam(String stringHero) {
        for (Hero hero : Hero.values()) {
            if (hero.toString().toLowerCase().equals(stringHero)) {
                return hero;
            }
        }

        return Hero.TROLL;
    }
}
