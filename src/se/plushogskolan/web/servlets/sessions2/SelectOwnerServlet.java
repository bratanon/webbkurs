package se.plushogskolan.web.servlets.sessions2;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

public class SelectOwnerServlet extends AbstractSessionServlet {

	private static final long serialVersionUID = 1L;
	private Logger log = Logger.getLogger(SelectOwnerServlet.class);

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		log.debug("SelectOwnerServlet.doGet");

		// Find social security number in either session or cookie

		// If social security number is found, redirect to selectCar.servlet

		// Otherwise get an owner and forward to selectOwner.jsp

	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		log.debug("SelectOwnerServlet.doPost");

		// Set the owner in both a session attribute and a cookie.

	}

}
