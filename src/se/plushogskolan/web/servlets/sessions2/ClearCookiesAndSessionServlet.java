package se.plushogskolan.web.servlets.sessions2;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

public class ClearCookiesAndSessionServlet extends AbstractSessionServlet {

	private static final long serialVersionUID = 1L;
	private Logger log = Logger.getLogger(ClearCookiesAndSessionServlet.class);

	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		// Although it is not a good practice, also let GET requests delete
		// cookies and session values. This allows
		// us to use links and not forms when making the request.
		doPost(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		// This method should do 3 things
		// - On parameter "clearAll", delete all session attributes and cookies
		// - On parameter "clearCookie", delete the cookie with the given name
		// - On parameter "clearSessionAttribute", delete the session attribute
		// with the given name

		// After the delete, redirect to /selectOwner.servlet

	}

}
