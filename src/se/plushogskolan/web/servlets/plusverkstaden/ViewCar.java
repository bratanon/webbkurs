package se.plushogskolan.web.servlets.plusverkstaden;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import se.plushogskolan.web.domain.cars.Car;
import se.plushogskolan.web.domain.cars.CarService;

/**
 * Servlet implementation class ViewCar
 */
public class ViewCar extends HttpServlet {
    private static final long serialVersionUID = 1L;

    private Logger log = Logger.getLogger(this.getClass().getName());

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
     *      response)
     */
    // TODO: Fix error handling here.
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {

        RequestDispatcher dispatcher = request
                .getRequestDispatcher("/WEB-INF/jsp/plusverkstaden/viewCar.jsp");

        String paramRegNo = request.getParameter("regNo");

        // Get specified car.
        Car car = CarService.getInstance().getCarByRegNo(paramRegNo);
        request.setAttribute("car", car);

        dispatcher.forward(request, response);
    }
}
