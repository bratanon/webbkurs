package se.plushogskolan.web.servlets.plusverkstaden;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import se.plushogskolan.web.domain.cars.Brand;
import se.plushogskolan.web.domain.cars.Car;
import se.plushogskolan.web.domain.cars.CarService;
import se.plushogskolan.web.domain.cars.Owner;
import se.plushogskolan.web.util.CookieWrapper;
import se.plushogskolan.web.util.SessionWrapper;

/**
 * Servlet implementation class CarForm
 */
public class CarForm extends HttpServlet {
    private static final long serialVersionUID = 1L;

    private Logger log = Logger.getLogger(this.getClass().getName());

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
     *      response)
     */
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {

        request.setAttribute("brands", Brand.values());
        request.setAttribute("owners", CarService.getInstance().getOwners());
        request.setAttribute("op", "add");

        String regNo = request.getParameter("regNo");

        Car car = CarService.getInstance().getCarByRegNo(regNo);

        if (car != null) {
            request.setAttribute("car", car);
            request.setAttribute("op", "update");
        }

        RequestDispatcher dispatcher = request
                .getRequestDispatcher("/WEB-INF/jsp/plusverkstaden/carForm.jsp");
        dispatcher.forward(request, response);

    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
     *      response)
     */
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {

        request.setAttribute("brands", Brand.values());
        request.setAttribute("owners", CarService.getInstance().getOwners());
        request.setAttribute("op", request.getParameter("op"));

        ArrayList<String> errors = new ArrayList<String>();

        String op = request.getParameter("op");

        String regNo = "";
        int year = 0;
        double fuelConsumption = 0;
        Owner owner = null;
        Brand brand = null;

        if (request.getParameter("regno").trim().isEmpty()) {
            errors.add("Missing reg no");
        }
        else {
            regNo = request.getParameter("regno").trim();
        }

        if (request.getParameter("year").trim().isEmpty()) {
            errors.add("Missing year");
        }
        else {
            try {
                year = Integer.parseInt(request.getParameter("year"));

                if (year > 2013 || year < 1970) {
                    errors.add("Invalid year, must e between 1970 and 2013");
                }
            }
            catch (NumberFormatException e) {
                errors.add("Invalid year, must e between 1970 and 2013");
            }

        }

        if (request.getParameter("fuelConsumption").trim().isEmpty()) {
            errors.add("Missing fuel consumption");
        }
        else {
            try {
                fuelConsumption = Double.parseDouble(request
                        .getParameter("fuelConsumption"));
                if (fuelConsumption > 3 || fuelConsumption < 0.1) {
                    errors.add("Invalid fuel consumption, must be between 0.1 and 3");
                }

                // Fake dummy exception.
                if (fuelConsumption == 0.01) {
                    throw new ServletException(
                            "Dummy exception from CarFrom servlet.");
                }

            }
            catch (NumberFormatException e) {
                errors.add("Invalid fuel consumption, must be between 0.1 and 3");
            }
        }

        if (request.getParameter("brand").trim().isEmpty()) {
            errors.add("Missing brand");
        }
        else {
            brand = Brand.valueOf(request.getParameter("brand"));
        }

        if (request.getParameter("owner").trim().isEmpty()) {
            errors.add("Missing owner");
        }
        else {
            owner = CarService.getInstance().getOwnerBySocialSecurityNo(
                    request.getParameter("owner"));
        }

        if (errors.size() > 0) {
            request.setAttribute("errors", errors);
            request.setAttribute("op", op);
            RequestDispatcher dispatcher = request
                    .getRequestDispatcher("/WEB-INF/jsp/plusverkstaden/carForm.jsp");
            dispatcher.forward(request, response);
        }
        else {
            Car new_car = new Car(regNo, year, brand, fuelConsumption, owner);
            CarService.getInstance().createOrUpdateCar(new_car);

            SessionWrapper.add(request, "edited_cars", regNo);

            response.addCookie(CookieWrapper.add(request, "edited_cars", regNo));

            response.sendRedirect(response.encodeRedirectURL(request
                    .getContextPath() + "/plusverkstaden/listCars.servlet"));
        }
    }
}
