package se.plushogskolan.web.servlets.sessions1;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

/**
 * Servlet implementation class setSessionValues
 */
public class setSessionValues extends HttpServlet {
    private static final long serialVersionUID = 1L;

    private Logger log = Logger.getLogger(this.getClass().getName());

    private Map<String, String> languages = new HashMap<String, String>();

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
     *      response)
     */
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {

        languages.put("sv", "Svenska");
        languages.put("de", "Deutsch");
        languages.put("en", "English");

        request.setAttribute("languageList", languages);

        RequestDispatcher dispatcher = request
                .getRequestDispatcher("/WEB-INF/jsp/sessions1/setSessionValues.jsp");
        dispatcher.forward(request, response);
    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
     *      response)
     */
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {
        String name = request.getParameter("name");
        String language = request.getParameter("language");

        if (name != null && !name.isEmpty()) {
            request.getSession().setAttribute("name", name);
        }

        if (language != null) {
            Cookie langCoockie = new Cookie("language", languages.get(language));
            response.addCookie(langCoockie);
        }

        response.sendRedirect(response.encodeRedirectURL(request
                .getContextPath()
                + "/servletexercises/jsp/sessions1/readSessionValues.jsp"));

    }

}
