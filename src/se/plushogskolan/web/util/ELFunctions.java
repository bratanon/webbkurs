package se.plushogskolan.web.util;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

public class ELFunctions {
    /**
     * Gets the current date.
     * 
     * @param pattern
     *            the pattern describing the date and time format
     * @return the current date in the specified pattern.
     */
    public static String getDate(String pattern) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(pattern);
        return dateFormat.format(new Date());
    }

    public static int getRandomNumber() {
        Random rand = new Random();
        return rand.nextInt(99999);
    }
}
