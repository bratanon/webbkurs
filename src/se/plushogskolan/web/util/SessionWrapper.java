package se.plushogskolan.web.util;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

public class SessionWrapper {

    private static Logger log = Logger.getLogger(SessionWrapper.class);

    @SuppressWarnings("unchecked")
    public static void add(HttpServletRequest request, String name, String value) {
        List<String> list = (List<String>) request.getSession().getAttribute(
                name);

        if (list == null) {
            list = new ArrayList<String>();
        }

        if (list.contains(value)) {
            list.remove(value);
        }

        list.add(value);

        request.getSession().setAttribute(name, list);
    }
}
