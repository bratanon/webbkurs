package se.plushogskolan.web.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;

public class CookieWrapper {

    public static Cookie add(HttpServletRequest request, String name,
            String value) {
        Cookie cookie = CookieWrapper.getCookieByName(request, name);

        boolean isNewCookie = false;

        if (cookie == null) {
            cookie = new Cookie(name, "");
            cookie.setValue(value);
            isNewCookie = true;
        }

        // Set new TTL to 7 days.
        cookie.setMaxAge((3600 * 24) * 7);

        if (isNewCookie) {
            return cookie;
        }

        String currentValue = cookie.getValue();

        List<String> list = new ArrayList<>(Arrays.asList(currentValue
                .split(":")));

        if (list.contains(value)) {
            list.remove(value);
        }

        list.add(value);
        cookie.setValue(StringUtils.join(list, ":"));

        return cookie;

    }

    public static Cookie getCookieByName(HttpServletRequest request, String name) {
        Cookie[] cookies = request.getCookies();

        if (cookies == null) {
            return null;
        }

        for (Cookie cookie : cookies) {
            if (cookie.getName().equals(name)) {
                return cookie;
            }
        }

        return null;
    }
}
