package se.plushogskolan.demo;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

import org.apache.log4j.Logger;

public class UdpServer {

    private static Logger log = Logger.getLogger(UdpServer.class);

    public static void main(String[] args) throws Exception {
        log.debug("UdpServer starting up...");

        DatagramSocket serverSocket = new DatagramSocket(9091);
        byte[] receiveData = new byte[1024];
        byte[] sendData = new byte[1024];

        while (true) {

            // Receive
            DatagramPacket receivePacket = new DatagramPacket(receiveData,
                    receiveData.length);
            serverSocket.receive(receivePacket);
            String input = new String(receivePacket.getData());
            log.debug("Received datagram: " + input);

            // Send
            InetAddress IPAddress = receivePacket.getAddress();
            int port = receivePacket.getPort();
            String response = "Hello, " + input;
            sendData = response.getBytes();
            DatagramPacket sendPacket = new DatagramPacket(sendData,
                    sendData.length, IPAddress, port);
            serverSocket.send(sendPacket);
        }
    }

}
