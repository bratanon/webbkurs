package se.plushogskolan.demo;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

import org.apache.log4j.Logger;

public class UdpClient {

    private static Logger log = Logger.getLogger(UdpClient.class);

    public static void main(String args[]) throws Exception {

        DatagramSocket clientSocket = new DatagramSocket();
        InetAddress ip = InetAddress.getByName("localhost");
        byte[] sendData = new byte[1024];
        byte[] receiveData = new byte[1024];

        sendData = new String("Bertil").getBytes();
        DatagramPacket sendPacket = new DatagramPacket(sendData,
                sendData.length, ip, 9091);
        clientSocket.send(sendPacket);

        DatagramPacket receivePacket = new DatagramPacket(receiveData,
                receiveData.length);
        clientSocket.receive(receivePacket);
        String response = new String(receivePacket.getData());
        log.debug("Response from server:" + response);
        clientSocket.close();
    }
}