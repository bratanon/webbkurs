package se.plushogskolan.demo;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

import org.apache.log4j.Logger;

public class HttpConnectionDemo {

    private static Logger log = Logger.getLogger(HttpConnectionDemo.class);

    public static void main(String[] args) throws Exception {
        URL url = new URL("http://www.aftonbladet.se");
        URLConnection connection = url.openConnection();

        connection.connect();

        InputStream inputStream = connection.getInputStream();
        BufferedReader reader = new BufferedReader(new InputStreamReader(
                inputStream));
        String inputLine;

        while ((inputLine = reader.readLine()) != null) {
            log.debug(inputLine);
        }

        inputStream.close();
    }
}
