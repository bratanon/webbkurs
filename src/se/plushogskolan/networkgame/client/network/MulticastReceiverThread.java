package se.plushogskolan.networkgame.client.network;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;

import se.plushogskolan.networkgame.client.GameClient;

/**
 * A thread to receive multicast messages.
 * 
 */
public class MulticastReceiverThread extends Thread {
    private static Logger log = Logger.getLogger(MulticastReceiverThread.class);
    private int port;
    private String ip;

    public MulticastReceiverThread (String ip, int port) {
        log.debug("Starting MulticastReceiverThread: ip=" + ip + ", port="
                + port);
        this.port = port;
        this.ip = ip;
    }

    public void run() {
        MulticastSocket socket = null;
        InetAddress address = null;
        try {

            while (true) {

                socket = new MulticastSocket(this.port);
                address = InetAddress.getByName(this.ip);
                socket.joinGroup(address);

                byte[] buf = new byte[256];
                DatagramPacket packet = new DatagramPacket(buf, buf.length);
                socket.receive(packet);

                String multicastMsg = new String(packet.getData(), 0,
                        packet.getLength());

                log.debug("Received message in " + this.getClass().getName()
                        + ": " + multicastMsg);

                if (multicastMsg.startsWith("pot:")) {
                    int pot = parsePot(multicastMsg);
                    GameClient.getInstance().handleUpdatedPot(pot);
                }
            }

        }
        catch (IOException e) {
            e.printStackTrace();
        }
        finally {
            try {
                socket.leaveGroup(address);
                socket.close();
            }
            catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

    /**
     * Parse a pot message using regular expressions
     */
    private int parsePot(String input) {
        Pattern pattern = Pattern.compile("pot:(\\d*)");
        Matcher matcher = pattern.matcher(input);
        if (matcher.matches()) {
            return Integer.parseInt(matcher.group(1));
        }
        else {
            throw new RuntimeException("Could not parse pot for input: "
                    + input);
        }

    }

}
