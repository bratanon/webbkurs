package se.plushogskolan.networkgame.client.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import se.plushogskolan.networkgame.client.domain.GameSession;

/**
 * A filter that checks if the session contains valid game data, otherwise the
 * user has to login in.
 * 
 */
public class CheckActiveSessionFilter implements Filter {

	private static Logger log = Logger.getLogger(CheckActiveSessionFilter.class);

	// Ignore this filter for certain URLs.
	private String ignoreUrlPattern;

	@Override
	public void doFilter(ServletRequest req, ServletResponse resp, FilterChain filterChain) throws IOException,
			ServletException {

		log.debug("Entering CheckActiveSessionFilter");

		HttpServletRequest httpReq = (HttpServletRequest) req;

		// Check if this filter should be ignored for this URL
		if (shouldFilterBeIgnored(httpReq)) {
			log.debug("Ignoring filter for url " + httpReq.getRequestURI());
			filterChain.doFilter(req, resp);
			return;
		}

		// Check if GameSession exists in user�s session. If not, return to the
		// login page
		GameSession gameSession = (GameSession) httpReq.getSession().getAttribute("gameSession");
		if (gameSession == null) {
			log.debug("No GameSession found in session, redirect to login page");
			redirectToLogin(httpReq, (HttpServletResponse) resp);
			return;
		}

		// Check if logout parameter exists. In that case invalidate the session
		// and return to the login page
		String logoutParam = req.getParameter("logout");
		if ("true".equals(logoutParam)) {
			log.debug("Logout parameter found, redirect to login page");
			httpReq.getSession().invalidate();
			redirectToLogin(httpReq, (HttpServletResponse) resp);
			return;
		}

		filterChain.doFilter(req, resp);

	}

	private boolean shouldFilterBeIgnored(HttpServletRequest req) {
		if (req.getRequestURI().contains(ignoreUrlPattern)) {
			return true;
		}

		if (!req.getRequestURI().contains(".servlet")) {
			return true;
		}

		return false;
	}

	private void redirectToLogin(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		resp.sendRedirect(req.getContextPath() + "/networkgame/client/register.servlet");
	}

	@Override
	public void init(FilterConfig config) throws ServletException {
		this.ignoreUrlPattern = config.getInitParameter("ignoreUrl");
	}

	@Override
	public void destroy() {

	}

}
