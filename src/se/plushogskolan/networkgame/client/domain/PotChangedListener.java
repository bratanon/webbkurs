package se.plushogskolan.networkgame.client.domain;

/**
 * Interface for classes that wants to be notified when the pot changes.
 * 
 */
public interface PotChangedListener {

	void onPotChanged(int newPot);

}
