package se.plushogskolan.networkgame.client.domain;

/**
 * An object that represents a game session for one player. Used to store
 * information needed on the client side. The core game logic is located in the
 * game server, however.
 * 
 */
public class GameSession {
	private int playerId;
	private String playerName;
	private int credit;
	private int amountLastBet;

	public GameSession(String playerName, int playerId, int amount) {
		setPlayerName(playerName);
		setPlayerId(playerId);
		setCredit(amount);
	}

	public int getPlayerId() {
		return playerId;
	}

	public void setPlayerId(int playerId) {
		this.playerId = playerId;
	}

	public void setCredit(int credit) {
		this.credit = credit;
	}

	public int getCredit() {
		return credit;
	}

	public String getPlayerName() {
		return playerName;
	}

	public void setPlayerName(String name) {
		this.playerName = name;
	}

	public int getAmountLastBet() {
		return amountLastBet;
	}

	public void setAmountLastBet(int amountLastBet) {
		this.amountLastBet = amountLastBet;
	}

	@Override
	public String toString() {
		return "GameSession [playerId=" + playerId + ", name=" + playerName + ", credit=" + credit + "]";
	}

}
