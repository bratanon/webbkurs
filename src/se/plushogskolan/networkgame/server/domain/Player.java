package se.plushogskolan.networkgame.server.domain;

/**
 * A player on the server side only stores the id and the amount of money that
 * player has.
 * 
 */
public class Player {

	private static int idCounter = 1;

	private int id;
	private int amount;

	public Player() {
	}

	public static Player createPlayerWithGeneratedId() {
		return new Player(idCounter++);
	}

	public void withDrawAmount(int amountToWithdraw) {
		setAmount(getAmount() - amountToWithdraw);
	}

	public void addWin(int win) {
		setAmount(getAmount() + win);
	}

	public Player(int id) {
		setId(id);
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}

	public int getAmount() {
		return amount;
	}

	@Override
	public String toString() {
		return "Player [id=" + id + ", amount=" + amount + "]";
	}

}
