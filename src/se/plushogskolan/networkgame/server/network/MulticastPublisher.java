package se.plushogskolan.networkgame.server.network;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

import org.apache.log4j.Logger;

/**
 * A class that can send messages to a multicast address, thus notifying many
 * clients at the same time.
 * 
 */
public class MulticastPublisher {
    private static Logger log = Logger.getLogger(MulticastPublisher.class);
    protected DatagramSocket socket = null;
    private String ip;
    private int port;

    public MulticastPublisher (String ip, int port) throws IOException {
        log.debug("MulticastPublisher: ip=" + ip + ", port=" + port);
        this.ip = ip;
        this.port = port;
        socket = new DatagramSocket();
    }

    public void sendMessage(String msg) {

        try {
            log.debug("MulticastPublisher SendMessage");
            // Create a DatagramPacket and send it using the DatagramSocket.
            // The IP and port are provided in the constructor.

            byte[] buf = msg.getBytes();

            // Send message
            InetAddress ip = InetAddress.getByName("230.0.0.1");
            DatagramPacket packet = new DatagramPacket(buf, buf.length, ip,
                    port);
            socket.send(packet);

        }
        catch (Exception e) {
            throw new RuntimeException("Exception sending multicast message: "
                    + e.getMessage(), e);
        }
    }

}
