(function($) {
	$(document).ready(function() {
		$('#speed').change(function() {
			$('#speedControll label span').text('(' + $(this).val() + ')');
		})
		.mouseup(function() {
			window.location = buildUrl('speed=' + $(this).val());
		});
		
		$(document).keyup(function(e) {
			console.log(e.which);
			switch (e.which) {
			case 37:
				// left
				window.location = buildUrl('dir=left');
				break;
			case 38:
				// up
				window.location = buildUrl('dir=up');
				break;
			case 39:
				// right
				window.location = buildUrl('dir=right');
				break;
			case 40:
				// down
				window.location = buildUrl('dir=down');
				break;
			}
		});

		function buildUrl(queryString) {
			var base = window.location.href.split('?')[0];
			return base + '?' + queryString;
		}
		
	});
})(jQuery);