<%@ taglib prefix="pvn" uri="se.plushogskolan.plusverkstaden" %>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
</head>
<body>

	<h1>Custom tags</h1>
	<pvn:getCar regNo="ABC-123" carAttributeName="car">
		<p>The owner if car ${car.regNo} is ${car.owner.firstName} ${car.owner.lastName} who lives in ${car.owner.city}.</p>
		<p>The car ${car.regNo} from ${car.brand} has a fuel consumption of ${car.fuelConsumption} l / 10km.</p>
	</pvn:getCar>
	
	
</body>
</html>