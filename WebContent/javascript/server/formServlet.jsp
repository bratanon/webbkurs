<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

<%-- 
	This page represents some kind of servlet functionality. It responds to a form submission, 
	and prints some basic JSON data back to the browser. 
--%>

 {"name":"${param.name }", "age": "${param.age}" }  
    