<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ attribute name="items" required="true" type="java.util.List" %>
<table class="car-list">
	<tr>
		<th>Reg no</th>
		<th>Year</th>
		<th>Brand</th>
		<th>Fuel consumption</th>
		<th>Owner</th>
		<th class="operations">Operations</th>
	</tr>
	<c:forEach items="${items}" var="item">
		<tr>
			<td><c:out value="${item.regNo}" /></td>
			<td><c:out value="${item.yearModel}" /></td>
			<td><c:out value="${item.brand.niceName}" /></td>
			<td><c:out value="${item.fuelConsumption }" /></td>
			<td>
				<c:out value="${item.owner.fullName}" />
			</td>
			<td class="operations">
				<div class="btn-group">
				  <i class="fa fa-cog"></i> Operations <i class="fa fa-caret-down"></i>
				  <ul class="btn-dropdown">
				    <li><a href="<c:url value="viewCar.servlet?regNo=${item.regNo}" />"><i class="fa fa-tachometer fa-fw"></i> Show</a></li>
				    <li><a href="<c:url value="carForm.servlet?regNo=${item.regNo}" />"><i class="fa fa-pencil fa-fw"></i> Edit</a></li>
				  </ul>
				</div>
			</td>
		</tr>
	</c:forEach>
</table>