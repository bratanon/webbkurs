<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page import="java.util.*"%>

<html>

<head>
<link rel="stylesheet"
	href="<%=request.getContextPath()%>/styles/global.css">
</head>

<body>

	<h1>Request response</h1>

	<p>This exercise shows the difference between GET and POST
		requests. It also shows the values that are sent as parameters,
		attributes and headers.</p>

	<ul class="noBullets">
		<li>Request method: <b><% out.print(request.getMethod()); %></b></li>
		<li>Response status code: <b><% out.print(response.getStatus()); %></b></li>
		<li>Response content type: <b><% out.print(response.getContentType()); %></b></li>
	</ul>

	<p>
		<b>This is a list of all parameters:</b>
	</p>
	<table class="dataTable">
		<tr>
			<th>Parameter name</th>
			<th>Parameter value</th>
		</tr>
		<%
			Map<String, String[]> reqParams = request.getParameterMap();
			for (Map.Entry<String, String[]> param : reqParams.entrySet()) {
				for (String paramValue : param.getValue()) {
					out.println("<tr><td>" + param.getKey() + "</td><td>" + paramValue + "</td></tr>");
				}
			}
		%>
	</table>

	<p>
		<b>This is a list of all attributes:</b>
	</p>
	<table class="dataTable">
		<tr>
			<th>Attribute name</th>
			<th>Attribute value</th>
		</tr>
		<%
			Enumeration<String> reqAttrs = request.getAttributeNames();
			while (reqAttrs.hasMoreElements()) {
				String attrName = reqAttrs.nextElement().toString();
				out.println("<tr><td>" + attrName + "</td><td>" + request.getAttribute(attrName) + "</td></tr>");
			}
		%>
	</table>

	<p>
		<b>This is a list of all request headers:</b>
	</p>
	<table class="dataTable">
		<tr>
			<th>Header name</th>
			<th>Header value</th>
		</tr>
		<%
			Enumeration<String> reqHeaders = request.getHeaderNames();
			while (reqHeaders.hasMoreElements()) {
				String headerName = reqHeaders.nextElement().toString();
				out.println("<tr><td>" + headerName + "</td><td>" + request.getHeader(headerName) + "</td></tr>");
			}
		%>
	</table>

	<p>
		<b>This is a list of all response headers:</b>
	</p>
	<table class="dataTable">
		<tr>
			<th>Header name</th>
			<th>Header value</th>
		</tr>
		<%
			Collection<String> resHeaders = response.getHeaderNames();
			Iterator<String> resHeadersIterator = resHeaders.iterator();

			while (resHeadersIterator.hasNext()) {
				String headerName = resHeadersIterator.next();
				out.println("<tr><td>" + headerName + "</td><td>" + response.getHeader(headerName) + "</td></tr>");
			}
		%>
	</table>




	<h2>GET requests</h2>

	<ul class="noBullets">

		<li><a
			href="<%=request.getContextPath()%>/requestResponse.servlet?id=1">A
				link with an ID parameter = 1</a></li>
		<li><a
			href="<%=request.getContextPath()%>/requestResponse.servlet?id=2">A
				link with an ID parameter = 2</a></li>
		<li><a
			href="<%=request.getContextPath()%>/requestResponse.servlet?name=Kalle">A
				link with a name parameter</a></li>
		<li><a
			href="<%=request.getContextPath()%>/requestResponse.servlet?x=234&y=234&length=900&speed=145">A
				link with many parameters</a></li>

	</ul>

	<h2>POST requests</h2>
	<form action="<%=request.getContextPath()%>/requestResponse.servlet"
		method="POST">

		<table class="dataTable">
			<tr>
				<th>Name</th>
				<td><input type="text" name="name"></td>
			</tr>
			<tr>
				<th>Street</th>
				<td><input type="text" name="street"></td>
			</tr>
			<tr>
				<th>Phone</th>
				<td><input type="text" name="phone" size="20"></td>
			</tr>


		</table>
		<input type="submit" value="Submit using POST">
	</form>
</body>

</html>