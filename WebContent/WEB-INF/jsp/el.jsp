<%@ page import="se.plushogskolan.*" %>

<!DOCTYPE html>

<html>
<head>
	<meta charset="utf-8" />
	<link rel="stylesheet" href="<%= request.getContextPath() %>/styles/global.css">
</head>
<body>

	<h1>EL - Expression language</h1>

	<h2>Printing the values of a car</h2>

	<table class="dataTable">
		<tr>
			<th colspan="2">Car</th>
		</tr>
		<tr>
			<td>Reg number</td>
			<td>${car.regNo}</td>
		<tr>
		<tr>
			<td>Year model</td>
			<td>${car.yearModel}</td>
		<tr>
		<tr>
			<td>Brand</td>
			<td>${car.brand}</td>
		<tr>
		<tr>
			<td>Fuel consumption</td>
			<td>${car.fuelConsumption}</td>
		</tr>
		<tr>
			<th colspan="2">Owner</th>
		</tr>
		<tr>
			<td>First name</td>
			<td>${car.owner.firstName}</td>
		<tr>
		<tr>
			<td>Last name</td>
			<td>${car.owner.lastName}</td>
		<tr>
		<tr>
			<td>Address</td>
			<td>${car.owner.address}</td>
		<tr>
		<tr>
			<td>Zip code</td>
			<td>${car.owner.zipCode}</td>
		<tr>
		<tr>
			<td>City</td>
			<td>${car.owner.city}</td>
		<tr>
	</table>

	
	<h2>Printing parameters</h2>

	<p>Enter the name of your favorite pet. Below you should print the
		pet's name using the sent request parameter.</p>

	<form action="<%= request.getContextPath() %>/expressionLanguage.servlet">
		<input type="text" name="pet" />
		<input type="submit" value="Send pet name" />
	</form>

	<p>Your favorite pet is: ${param.pet}</p>

	<h2>Printing variables in different scope</h2>

	<dl>
		<dt>Request</dt>
		<dd>${requestScope.message}</dd>
		<dt>Session</dt>
		<dd>${sessionScope.message}</dd>
		<dt>Application</dt>
		<dd>${applicationScope.message}</dd>
	</dl>

	<h2>Printing headers</h2>
	
	<dl>
		<dt>Host</dt>
		<dd>${header.host}</dd>
	</dl>
	
	<h2>Printing values from the request</h2>
	<dl>
		<dt>URI</dt>
		<dd>${pageContext.request.requestURI}</dd>
		<dt>Method</dt>
		<dd>${pageContext.request.method}</dd>
	</dl>

</body>

</html>