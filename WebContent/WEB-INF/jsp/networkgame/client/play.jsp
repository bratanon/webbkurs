<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Play</title>
<link rel="stylesheet"
	href="<c:url value="/networkgame/networkgame.css" />">

<script src="<c:url value="/networkgame/jquery-1.10.1.min.js" />"></script>
<script src="<c:url value="/networkgame/jquery-migrate-1.2.1.min.js" />"></script>

<script type="text/javascript">
	$(document).ready(function() {

		startWebSockets();

		$("#playForm").submit(function(event) {
			event.preventDefault();
			var $playButton = $("#playButton");
			$playButton.attr("disabled", "disabled");
		
			$.post($(this).attr("action"), $(this).serialize(), function(data) {
				  var $el = $("#msgText");
				  $el.fadeOut(600, function() {
					  if (data.win > 0) {
						  $el.text("You won " + data.win + "SEK")
						  .removeClass("lost")
						  .addClass("win");
					  }
					  else {
						  $el.text("Sorry, you lost")
						  .removeClass("win")
						  .addClass("lost");
					  }
					  
					  $el.fadeIn(400);
					  $("#credit").text(data.credit);
					  $playButton.removeAttr("disabled");
					  
				  });

		   }, "json");
		});

	});

	function startWebSockets() {
		var socket = new WebSocket("ws://localhost:8080<%=request.getContextPath() %>/networkgame/client/websocket.servlet");

	     socket.onmessage = function (event) { 
	        var json = JSON.parse(event.data);
	        console.log(JSON.parse(event.data));
	         $("#potsize").text(json.pot);
	     };
	}
</script>

</head>
<body class="play">

	<div class="outerFrame">

		<header>
			<div class="headerTitle">
				<h1>PlusCasino</h1>
			</div>
			<div class="headerCredit">
				<p>Your credit: <span id="credit">${sessionScope.gameSession.credit}</span> SEK</p>
				<p>
					Pot size: <span id="potsize">?</span>
				</p>
				<p>
					<a href="?logout=true">Log out</a>
				</p>
			</div>

		</header>


		<div id="msgDiv">
			<p id="msgText">Come on, try your luck!</p>
		</div>


		<form id="playForm"
			action="<%=request.getContextPath()%>/networkgame/client/play.servlet"
			method="POST">
			<p>

				Amount:<br> <input type="text" name="amount" id="amountField"
					size="6" value="${gameSession.amountLastBet }" /> <input
					type="submit" value="Play" id="playButton">
			</p>


		</form>

	</div>

</body>
</html>