<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Register</title>

<link rel="stylesheet"
	href="<c:url value="/networkgame/networkgame.css" />">

<script type="text/javascript">
	function checkForm() {

		/* 
			Validate name and amount. If any validation criteria is not met, use an alert
			to notify the user, and make this method return false. Returning false
			will stop the form submission. 
		
		 */
		 var nameElement = document.getElementById("nameInput");
		 var amountElement = document.getElementById("amount");
		 
		 if (nameElement.value == "") {
			 alert("You need to enter a name");
			 return false;
		 }
		
		 if (amountElement.value == "") {
			 alert("You need to enter a amount to start with");
			 return false;
		 }
		 
		 if (isNaN(amountElement.value)) {
			 alert("Amount to start with most be a number");
			 return false;
		 }
		 
		 return true;
	}
</script>


</head>
<body class="register">

	<div class="outerFrame">

		<h1>Welcome to PlusCasino</h1>

		<form
			action="<%=request.getContextPath()%>/networkgame/client/register.servlet"
			method="POST" onsubmit="checkForm();">

			<p>
				Name:<br> <input type="text" name="name" value="" size="20"
					id="nameInput">
			</p>
			<p>
				Amount to start with:<br> <input type="text" name="amount"
					value="" id="amount" size="6" maxlength="6">
			</p>

			<p>
				<input type="submit" value="Enter" onclick="return checkForm()">

			</p>

		</form>

	</div>


</body>
</html>