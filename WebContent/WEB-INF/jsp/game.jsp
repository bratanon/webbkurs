<%@page import="se.plushogskolan.web.domain.game.GameEngine"%>
<%@page import="se.plushogskolan.web.domain.game.Hero"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%	
	GameEngine game = (GameEngine) request.getAttribute("game");
	Hero hero = game.getHero();
%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
	<title>Game on!</title>
	<link rel="stylesheet" href="styles/reset.css" />
	<link rel="stylesheet" href="styles/game.css" />
	<style type="text/css">
		#container { width: <% out.print(game.getGameWidth()); %>px; }
		#game-area { height: <% out.print(game.getGameHeight()); %>px; }
		
		#hero {
			background-image: url('servletexercises/game/<% out.print(hero.getImageUrl()); %>');
			width:<% out.print(hero.getWidth()); %>px;
			height:<% out.print(hero.getHeigh()); %>px;
			
			top:<% out.print(game.getyPos() - (hero.getWidth() / 2)); %>px;
			left: <% out.print(game.getxPos() - (hero.getHeigh() / 2)); %>px;
		}
	</style>
	<script type="text/javascript" src="//code.jquery.com/jquery-1.10.1.min.js"></script>
	<script type="text/javascript" src="scripts/game.js"></script>
</head>
<body>
	<div id="container">
		<div id="game-area">
			<div id="hero"></div>
		</div>
		<div id="controls">
			<div id="arrows">
				<a href="game.servlet?dir=up" id="arrow-up"><img src="servletexercises/game/top.png" /></a>
				<a href="game.servlet?dir=right" id="arrow-right"><img src="servletexercises/game/right.png" /></a>	
				<a href="game.servlet?dir=down" id="arrow-down"><img src="servletexercises/game/down.png" /></a>
				<a href="game.servlet?dir=left" id="arrow-left"><img src="servletexercises/game/left.png" /></a>
			</div>
			<div id="speedControll">
				<label for="speed">Speed: <span>(<% out.print(game.getSpeed()); %>)</span></label>
				<input id="speed" type="range" min="0" max="50" step="5" value="<% out.print(game.getSpeed()); %>" />
			</div>
			<div id="heros">
				<% for (Hero h : Hero.values()) { %>
				<a href="game.servlet?hero=<% out.print(h.toString().toLowerCase()); %>">
				    <img src="servletexercises/game/<% out.print(h.getImageUrl()); %>" width="<% out.print(h.getWidth()); %>" height="<% out.print(h.getWidth()); %>"/>
				</a>
				<% } %>
			</div>
		</div>
	</div>
</body>
</html>