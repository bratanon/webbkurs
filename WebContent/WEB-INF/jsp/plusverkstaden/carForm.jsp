<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html class="no-js">
	<head>
		<%@ include file="head.jsp" %>
		<%@ include file="styles.jsp" %>
	</head>
<body class="page-car-form">
	
	<%@ include file="page-header.jsp" %>
	<%@ include file="navigation.jsp" %>
	
	<section id="main">
		<div class="clearfix">
			
			<c:set value="Add new car" var="pageTitle"/>
			<c:if test="${not empty car}">
				<c:set value="Edit car ${car.regNo}" var="pageTitle"/>
			</c:if>
			
			<jsp:include page="page-title.jsp">
				<jsp:param name="title" value="${pageTitle}" />
			</jsp:include>
			
			<c:forEach items="${errors}" var="error">
				<p>${error}</p>
			</c:forEach>
			
			<form action="<c:url value="/plusverkstaden/carForm.servlet" />" method="post">
				<div class="form-item">
					<label for="regno">Reg no</label>
					<c:if test="${not empty car}">
						${car.regNo}
					</c:if>
					<c:if test="${empty car}">
						<input type="text" name="regno" id="regno" value="" pattern="[a-zA-Z]{3}-[0-9]{3}" placeholder="ABC-123" />
					</c:if>
				</div>
				<div class="form-item">
					<label for="year">Year</label>
					<input type="number" name="year" id="year" value="${car.yearModel}" min="1970" max="2013" placeholder="2013" />
				</div>
				<div class="form-item">
					<label for="brand">Brand</label>
					<select name="brand" id="brand">
						<option value="">Select brand</option>
						<c:forEach items="${brands}" var="brand">
							<c:if test="${car.brand.niceName eq brand.niceName }">
								<c:set value="selected" var="selected" />
							</c:if>
							<option value="${brand}" ${selected}>${brand.niceName}</option>
							<c:remove var="selected"/>
						</c:forEach>
					</select>
				</div>
				<div class="form-item">
					<label for="fuelConsumption">Fuel consumption</label>
					<input type="text" name="fuelConsumption" id="fuelConsumption" value="${car.fuelConsumption}" placeholder="1.0" /><span class="field-suffix">l / 10km</span>
				</div>
				<div class="form-item">
					<label for="owner">Owner</label>
					<select name="owner" id="owner">
						<option value="">Select owner</option>
						<c:forEach items="${owners}" var="owner">
							<c:if test="${car.owner.socialSecurityNo eq owner.socialSecurityNo }">
								<c:set value="selected" var="selected" />
							</c:if>
							<option value="${owner.socialSecurityNo}" ${selected}>${owner.fullName}</option>
							<c:remove var="selected"/>
						</c:forEach>
					</select>
				</div>

				<div class="form-actions">
					<c:set value="Add" var="submitTitle"/>
					<c:if test="${not empty car}">
						<c:set value="Update" var="submitTitle"/>
					</c:if>
					<input type="submit" name="submit" value="${submitTitle}" />
					<a href="<% out.print(request.getContextPath()); %>/plusverkstaden/listCars.servlet">Cancel</a>
				</div>
				<input type="hidden" name="op" value="${op}" />
				<c:if test="${not empty car}">
					<input type="hidden" name="regno" value="${car.regNo}" />
				</c:if>
			</form>
		</div>
	</section>
	
	<%@ include file="footer.jsp" %>
</body>
</html>