<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="pvn" uri="se.plushogskolan.plusverkstaden" %>
<link rel="stylesheet" href="<% out.print(request.getContextPath()); %>/theme/styles/reset.css" />
<link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">
<link rel="stylesheet" href="<% out.print(request.getContextPath()); %>/theme/styles/plusverkstaden.css?${pvn:getRandomNumber()}" />