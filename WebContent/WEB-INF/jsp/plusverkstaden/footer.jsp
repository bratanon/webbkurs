<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<footer id="footer">
	<div>
		<h1>Recently edited cars</h1>
		<ul>
			<c:forEach items="${edited_cars}" var="car">
				<li><a href="<c:url value="viewCar.servlet?regNo=${car.regNo}"/>">${car.regNo}  - ${car.owner.fullName}</a></li>
			</c:forEach>
		</ul>
	</div>
</footer>