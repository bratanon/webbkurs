<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="pvn-tag" tagdir="/WEB-INF/tags" %>

<!DOCTYPE html>
<html class="no-js">
	<head>
		<%@ include file="head.jsp" %>
		<%@ include file="styles.jsp" %>
	</head>
<body class="error-page">
	
	<%@ include file="page-header.jsp" %>
	<%@ include file="navigation.jsp" %>
	
	<section id="main">
		<div class="clearfix">
			<img src="<% out.print(request.getContextPath()); %>/images/404.jpg" alt="404 ERROR" />
			<jsp:include page="page-title.jsp">
				<jsp:param name="title" value="404 ERROR" />
			</jsp:include>
			
			<p><strong>Page not found</strong></p>
			<p>How about going back to the <a href="<c:url value="/plusverkstaden/listCars.servlet" />">start page?</a></p>
			
		</div>
	</section>
	
	<%@ include file="footer.jsp" %>
</body>
</html>