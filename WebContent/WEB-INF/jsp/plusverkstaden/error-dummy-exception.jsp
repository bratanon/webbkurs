<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="pvn-tag" tagdir="/WEB-INF/tags" %>
<%@ page isErrorPage="true" %>
<!DOCTYPE html>
<html class="no-js">
	<head>
		<%@ include file="head.jsp" %>
		<%@ include file="styles.jsp" %>
	</head>
<body class="error-page">
	
	<%@ include file="page-header.jsp" %>
	<%@ include file="navigation.jsp" %>
	
	<section id="main">
		<div class="clearfix">
			<img src="<% out.print(request.getContextPath()); %>/images/error.png" alt="ERROR" />
			<jsp:include page="page-title.jsp">
				<jsp:param name="title" value="Something bad happend" />
			</jsp:include>
			<p><strong>An exception was thrown:</strong></p>
			<p>${pageContext.exception}</p>
			
		</div>
	</section>
	
	<%@ include file="footer.jsp" %>
</body>
</html>