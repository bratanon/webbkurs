<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="pvn-tag" tagdir="/WEB-INF/tags" %>

<!DOCTYPE html>
<html class="no-js">
	<head>
		<%@ include file="head.jsp" %>
		<%@ include file="styles.jsp" %>
	</head>
<body>
	
	<%@ include file="page-header.jsp" %>
	<%@ include file="navigation.jsp" %>
	
	<section id="main">
		<div>
			<jsp:include page="page-title.jsp">
				<jsp:param name="title" value="List cars" />
			</jsp:include>

			<pvn-tag:listCars items="${cars}"/>
		</div>
	</section>
	
	<%@ include file="footer.jsp" %>
</body>
</html>