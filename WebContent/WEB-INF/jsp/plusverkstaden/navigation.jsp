<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="pvn" uri="se.plushogskolan.plusverkstaden" %>
<nav id="mainmenu" class="clearfix">
	<div>
		<ul>
			<li class="active"><a href="<c:url value="/plusverkstaden/listCars.servlet"/>" class="active">Home</a></li>
			<li><a href="<c:url value="/plusverkstaden/carForm.servlet"/>"> Add new car</a></li>
			<li><a href="<c:url value="/plusverkstaden/broken-url"/>">Broken URL</a></li>
		</ul>
		<span class="time">${pvn:getDate("Y-MM-dd HH:mm")}</span>
	</div>
</nav>