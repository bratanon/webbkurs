<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html class="no-js">
	<head>
		<%@ include file="head.jsp" %>
		<%@ include file="styles.jsp" %>
	</head>
<body class="page-view-car">
	
	<%@ include file="page-header.jsp" %>
	<%@ include file="navigation.jsp" %>
	
	<section id="main">
		<div class="clearfix">
			<jsp:include page="page-title.jsp">
				<jsp:param name="title" value="${car.regNo}" />
			</jsp:include>
			<section class="car-info">
				<table>
					<caption>Car information</caption>
					<tr>
						<th>Year model</th>
						<td><c:out value="${car.yearModel}" /></td>
					</tr>
					<tr>
						<th>Brand</th>
						<td><c:out value="${car.brand}" /></td>
					</tr>
					<tr>
						<th>Fuel consumption</th>
						<td><c:out value="${car.fuelConsumption}" /></td>
					</tr>
			</table>
			</section>
			
			<section class="car-owner-info">
				<table>
					<caption>Owner information</caption>
					<tr>
						<th>Name</th>
						<td><c:out value="${car.owner.firstName} ${car.owner.lastName}" /></td>
					</tr>
					<tr>
						<th>SSN</th>
						<td><c:out value="${car.owner.socialSecurityNo}" /></td>
					</tr>
					<tr>
						<th>Address</th>
						<td><c:out value="${car.owner.address}" /></td>
					</tr>
					<tr>
						<th>Zip code</th>
						<td><c:out value="${car.owner.zipCode}" /></td>
					</tr>
					<tr>
						<th>City</th>
						<td><c:out value="${car.owner.city}" /></td>
					</tr>
				</table>
			</section>
		</div>
	</section>
	
	<%@ include file="footer.jsp" %>
</body>
</html>