<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>

<link rel="stylesheet"	href="<c:url value="/styles/global.css" />">

<body>

	<jsp:include page="/servletexercises/jsp/sessions2/header.jsp" />

	<h1>Select car</h1>

	<form action='' method="POST">

		<p>[Drop down list with all cars]</p>

		<p>
			<input type="submit" value="Select car">
		</p>
	</form>
</body>

</html>